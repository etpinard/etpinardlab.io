# etpinard.xyz

Personal web page.

This site is hosted on GitLab Pages and built with yo-yo / tachyons / Hero Patterns.

## Deployment

Done via [`.gitlab-ci.yml`](.gitlab-ci.yml), which anticipates:

- a valid [github personal access token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)
  set as `GITHUB_TOKEN` in [CI/CD settings](https://gitlab.com/etpinard/etpinard.gitlab.io/-/settings/ci_cd)
  so that we can build the page content without hitting the API rate limit. Note
  that GitHub personal access token often expire in-between updates to this
  site.


## Tips

### Trick to shrink GIFs

```
sudo apt install gifsicle

# e.g
gifsicle old.gif "#0-70" -O3 --scale 0.5 --optimize --colors 16 -o new.gif

# 0-70 is the frame range
# find out how many frames there are in your gif using ImageMagick
identify old.gif
```

Or maybe try <https://github.com/ImageOptim/gifski>

## License

[MIT](./LICENSE)

[![Standard - JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)
