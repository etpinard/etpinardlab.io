var fs = require('fs-extra')
var path = require('path')
var yaml = require('js-yaml')
var request = require('request')
var parallel = require('run-parallel')
var series = require('run-series')
var Octokit = require('@octokit/core').Octokit
var marked = require('marked')
var escapeHtml = require('escape-html')
var trunc = require('../src/trunc')

var CVs = ['cv-en', 'cv-fr']
var BUILD = path.join(__dirname, '..', 'build')
var SRC = path.join(__dirname, '..', 'src')
var GITHUB_TOKEN = process.env.GITHUB_TOKEN || ''

if (GITHUB_TOKEN.length) {
  console.log('GITHUB_TOKEN found!')
} else {
  console.warn('Missing GITHUB_TOKEN in env, may run into API rate limit')
}

var octokit = new Octokit({ auth: GITHUB_TOKEN })
var noop = () => {}

var DATA
try {
  DATA = yaml.load(
    fs.readFileSync(path.join(SRC, 'data.yml'), 'utf8')
  )
} catch (err) {
  console.error(err)
}

var download = (uri, filename, cb) => {
  request.head(uri, (err, res, body) => {
    if (err) console.error(err)

    request(uri)
      .pipe(fs.createWriteStream(filename))
      .on('close', cb || noop)
  })
}

var GISTVIEW_REQUIRED = ['README.md', 'thumbnail.png', 'preview.gif']

var getGistRoot = (p) => `${DATA.rawgist.val}/${p.id}/raw/${p.commit}/`

var processInfo = (p, results) => {
  var out = { lookup: {} }

  results.forEach(res => {
    if (res.filename === '.block') {
      var blkOpts = {}
      res.body.split('\n').forEach(r => {
        var pieces = r.split(': ')
        blkOpts[pieces[0]] = pieces[1]
      })
      out.block = {
        height: parseInt(blkOpts.height) || 700,
        license: (blkOpts.license || '').toUpperCase() || 'none'
      }
    } else if (res.filename === 'README.md') {
      out.readme = marked.parse(res.body)
    } else if (p.files.indexOf(res.filename) !== -1) {
      out.lookup[res.filename] = res.filename.endsWith('.html')
        ? escapeHtml(res.body)
        : res.body
    }
  })

  return out
}

// (1) gistview content
DATA.posts
  .filter(p => p.type === 'gistview')
  .forEach(p => {
    var tid = trunc(p)
    var subDir = path.join(BUILD, tid)

    octokit.request(`GET /gists/${p.id}`).then(resp => {
      var files = Object.keys(resp.data.files)

      GISTVIEW_REQUIRED.forEach(req => {
        if (files.indexOf(req) === -1) {
          throw new Error(`Missing required file ${req} in post ${p.id}`)
        }
      })

      // (2a) create new gistview `build/` sub-directories
      var a = (cb) => fs.emptyDir(subDir, cb)

      // (2b) download gistview files
      var tasksDownload = files
        .map(filename => (cb) => {
          var root = getGistRoot(p)
          download(
            `${root}/${filename}`,
            path.join(subDir, filename),
            cb)
        })
      var b = (cb) => parallel(tasksDownload, cb)

      // (2c) process file `.info.json`
      var tasksProcess = files
        .filter(filename => (
          !filename.endsWith('.png') ||
          !filename.endsWith('.gif')))
        .map(filename => (cb) => {
          fs.readFile(path.join(subDir, filename), 'utf8', (err, body) => {
            var res = { filename: filename, body: body }
            cb(err, res)
          })
        })
      var c = (cb) => {
        parallel(tasksProcess, (err, results) => {
          if (err) console.error(err)
          fs.writeFile(
            path.join(BUILD, `${tid}.info.json`),
            JSON.stringify(processInfo(p, results)),
            cb)
        })
      }

      series([a, b, c])
    })
  })

// (2) download cv PNGs and PDFs
CVs
  .forEach((n) => {
    var d = DATA[n]
    var root = `https://gitlab.com/etpinard/cv/-/raw/${d.commit}/${d.folder}`
    download(`${root}/cv.png`, path.join(BUILD, `${n}.png`))
    download(`${root}/cv.pdf`, path.join(BUILD, `${n}.pdf`))
  })

// N.B. not equivalent to index.js version
var paragraph = (txt) => {
  var siteroot = DATA.site.val
  var lines = txt.split('\n\n')
  var line2dom = (l) => {
    var words = l.split(/(\s|[.,!?])/)
    return `<p>${words.map(word2dom).join('')}</p>`
  }
  var word2dom = (w) => {
    if (w.charAt(0) !== '@') return w
    var key = w.substr(1)
    var spec = DATA[key]
    var val = spec.val
    var content = spec.content || key
    switch (spec.type) {
      case 'link':
        if (!val.startsWith('https')) {
          val = `${siteroot}/${val}`
        }
        return `<a href="${val}">${content}</a>`
      case 'cv':
        return `<a href="${siteroot}#cv=true&lang=en">CV</a>`
      case 'reverse-str':
      case 'str':
        return val
    }
  }
  return `<p>${lines.map(line2dom).join('')}</p>\n`
}

// (3) create github profile readme content
fs.writeFile(
  path.join(BUILD, 'github-profile-readme.md'),
  paragraph(DATA['gh-readme']),
  (err) => { if (err) console.error(err) }
)
