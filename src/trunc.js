var isPlainObject = require('is-plain-object').isPlainObject

module.exports = function trunc (arg) {
  var out
  if (isPlainObject(arg)) {
    if (arg.id && arg.commit) {
      out = trunc(arg.id) + '-' + trunc(arg.commit)
    } else if (arg.id) {
      out = trunc(arg.id)
    }
  } else if (typeof arg === 'string') {
    out = arg.substr(0, 6)
  }

  if (!out) {
    throw new Error('Invalid argument')
  }
  return out
}
